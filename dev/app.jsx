import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
	render() {
		return (
			<p>Hello, {this.props.greeting}</p>
		);
	}
}

ReactDOM.render(
	<App greeting="Foozie" />,
	document.getElementById('container')
);